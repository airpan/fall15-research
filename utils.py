import cPickle
import numpy as np
import csv
import os
import os.path as osp
import urllib


def fetch_dataset(url):
    fname = download(url)
    extension = osp.splitext(fname)[-1]
    if extension == '.npz':
        return np.load(fname)
    elif extension == '.pkl':
        with open(fname, 'rb') as fin:
            return cPickle.load(fin)
    raise NotImplementedError


def download(url):
    fname = osp.basename(url)
    urllib.urlretrieve(url, fname)
    return fname


def load_file(file_path):
    with open(file_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        reader.next()
        rows = []
        for row in reader:
            rows.append(row)
        return rows
