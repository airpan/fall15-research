"""
The simplest Q-learning algorithm possible.
No batch size, just a linear function and samples.
"""
import itertools
import time

import numpy as np

import theano
import theano.tensor as T

floatX = theano.config.floatX

# Should really be in its own config file
state_dim = 2
n_features = 5
N = 51
gamma = 0.99
batch_size = 4
step_size = 0.1


def _linear_features(state):
    """ The identity. """
    return state


def _quadratic_features(state):
    """ Turns raw state into their quadratic features.

    Arguments:
        state: A numpy array of a batch of state

    Returns:
        The same batch of state once transformed
    """
    # Original state, state variables sqaures, choices of 2 different state
    obs = np.zeros(
        state_dim + state_dim + state_dim * (state_dim - 1) / 2
    )
    obs[:state_dim] = state
    obs[state_dim:2*state_dim] = state ** 2
    ind = 2 * state_dim
    for i, j in itertools.combinations(range(state_dim), 2):
        obs[ind] = state[i] * state[j]
        ind += 1
    return obs.astype(floatX)


featurizer = _quadratic_features


def linear(states, params):
    """ Linear Q-function of state.

    Since discrete actions may not have a reasonable
    interpretation, and I'd prefer avoiding direct indexing
    in Theano, this uses the same idea of implicitly implementing
    different actions in the number of outputs

    Arguments:
        states: Theano tensor for batch of states
        params: Dictionary of Theano tensors for parameters

    Returns:
        - an uncompiled Theano function that computes Q-values
        - a list of all params that can be updated
    """
    bias = params['bias']
    weights = params['weights']

    val = T.dot(states, weights) + bias
    return {
        'qvals': val,
    }


def quadratic_loss_fn(states, actions, rewards, qvals, params, regularizer):
    """ Quadratic loss between Q-val and one step lookahead.

    The tricky part here is that the params of the Q-value
    function need to be exposed to the loss function. So they
    can't be decoupled very easily.

    Arguments:
        states: Theano tensor for states
        actions: Theano tensor for actions
        rewards: Theano tensor for rewards
        qvals: A Q-value function that was defined with the same
            states tensor
        params: The params of that Q-function
        regularizer: There's a penalty term of lambda * (L2 distance from old param to new param)

    Returns a compiled quadratic loss function
    """
    # Turn dictionary into list of just tensors
    params = params.values()
    # Copy the starting values out of shared memory
    start_params = [
        param.get_value() for param in params
    ]
    # Construct penalty term
    distance = T.sum(
        [T.sum((param - start_value) ** 2) for (param, start_value) in zip(params, start_params)]
    )

    # qvals is of shape (batch_size, num_actions)
    # The ith row is the Q-values for the ith state of the batch, so
    # shift by 1
    cur_vals = qvals[:-1]
    next_vals = qvals[1:]

    best_vals = rewards + gamma * T.max(next_vals, axis=1)
    estimated_vals = next_vals[T.arange(batch_size), actions]
    # Average over batch and add penalty
    loss = T.mean(0.5 * (best_vals - estimated_vals) ** 2) + regularizer * distance

    grads = theano.gradient.grad(loss, params)
    updates = [(param, param - step_size * grad)
               for (param, grad) in zip(params, grads)]

    trainer = theano.function(
        [states, actions, rewards],
        loss,
        updates=updates,
    )

    # Return params too. This lets us save the parameters after training
    return {
        'loss': trainer,
    }


def run_episode(mdp, policy, start_state):
    """ Runs an episode of the mdp with the given policy and start_state. 

    Returns:
        The observed states, actions, and rewards. The states returned
        are already featurized.
    """
    states = []
    actions = []
    rewards = []

    state = start_state
    feat_state = featurizer(state)
    states.append(feat_state)

    # Cap episode length
    steps_left = 100
    while steps_left >= 0 and not mdp.should_end_episode(state):
        steps_left -= 1
        a = policy(feat_state)
        s, a, r, ns = mdp.next_state(state, [a])

        state = ns.astype(floatX)
        feat_state = featurizer(state)
        states.append(feat_state)
        actions.append(a)
        rewards.append(r)

    # Turn everything into numpy arrays
    states = np.array(states, dtype=floatX)
    actions = np.array(actions, dtype=np.int32)
    rewards = np.array(rewards, dtype=floatX)

    return {
        'states': states,
        'actions': actions,
        'rewards': rewards
    }


# TODO Do this properly with no dimension hacking
def eps_greedy_policy(qfunc, rng, eps=0.2):
    def pol(state):
        if rng.rand() < eps:
            return rng.randint(N)
        state = state.reshape(1, -1)
        vals = qfunc(state)[0]
        return np.argmax(vals)
    return pol


def _episode_reward(rewards):
    # Apply discounting to rewards array
    episode_length = len(rewards)
    discount = np.logspace(0., episode_length-1.,
                           base=gamma, num=episode_length)
    return np.sum(discount * rewards)


def q_learn(mdp, rng, start_state=None):
    """
    Pass in an LQRMDP and a numpy rng object.
    """
    states = T.matrix('states')
    actions = T.imatrix('actions')
    rewards = T.vector('rewards')

    # Initialize uniformly at random from (-0.1, 0.1)
    bias = np.array(0.2 * rng.rand() - 0.5).astype(floatX)
    bias = theano.shared(
        bias,
        name='bias',
    )
    weights = theano.shared(
        (0.2 * (rng.rand(n_features, N) - 0.5)).astype(floatX),
        name='weights'
    )
    params = {
        'bias': bias,
        'weights': weights,
    }

    # Raw q_values
    q_vals = linear(states, params)['qvals']
    # TODO run many rollouts in parallel
    compiled_q_vals = theano.function(
        [states],
        q_vals[0],
    )

    n_episodes = 1000
    if start_state is None:
        start_state = mdp.random_start_state().astype(floatX)

    re = []
    lo = []

    t = time.time()
    for i in xrange(n_episodes):
        policy = eps_greedy_policy(compiled_q_vals, rng)
        episode_info = run_episode(mdp, policy, start_state)
        print 'Finished episode %d' % i
        print 'Total time so far: %f seconds' % (time.time() - t)

        obs_states = episode_info['states']
        obs_actions = episode_info['actions']
        obs_rewards = episode_info['rewards']
        episode_length = len(obs_actions)

        loss_fn = quadratic_loss_fn(states, actions, rewards, q_vals, params, 0.01)['loss']
        losses = []
        # Compute the minimum. Could probably solve this exactly but for now
        # this uses gradient descent.
        prev_loss = 10 ** 5
        for _ in xrange(100):
            # If we don't have enough for a full batch, drop the leftover
            # TODO don't be as wasteful
            losses = []
            for i in xrange(0, episode_length - batch_size + 1, batch_size):
                losses.append(loss_fn(
                    obs_states[i:i+batch_size+1],
                    obs_actions[i:i+batch_size],
                    obs_rewards[i:i+batch_size],
                ))
        # Get reward and loss
        re.append(_episode_reward(obs_rewards))
        # Loss is averaged over all the batches in the episode
        # (Returns the average loss on the last iteration)
        lo.append(np.mean(losses))
        print 'Average reward: %f' % re[-1]
        print 'Average loss: %f' % lo[-1]

    return params, re, lo, start_state


if __name__ == '__main__':
    from lqr import LQRMDP
    import cPickle
    #A,B,Q,R = lqr.LQR_problem(2, 1)
    rng = np.random.RandomState(12345)
    #mdp = lqr.LQRMDP(A, B, Q, R, N, rng=rng)
    with open('lqr_results/mdp') as f:
        mdp = cPickle.load(f)
    with open('lqr_results/start_state') as f:
        start_state = cPickle.load(f).astype(floatX)
    params, re, lo, _ = q_learn(mdp, rng, start_state)
    with open('dumpingmdp2', 'w') as f:
        cPickle.dump(mdp, f)
        cPickle.dump(start_state, f)
    with open('dumpingground2', 'w') as f:
        cPickle.dump(params, f)
        cPickle.dump(re, f)
        cPickle.dump(lo, f)

