"""Visualize the action taken by the learned LQR network.

This assumes the problem is an MDP with 2 dimensional state
and 1 dimensional control.
"""
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import theano

import cPickle
import glob
import itertools
import os
import re
import time

# Must be imported from file to match cPickle's expectations
from lqr import LQRMDP
from lqr_deep_q import q_network

floatX = theano.config.floatX

def load_mdp(mdp_path):
    with open(mdp_path) as f:
        return cPickle.load(f)


def latest_net_path(results_dir):
    network_files = glob.glob('%s/network_file_*.pkl' % results_dir)
    last = max(
        network_files,
        key=lambda name: int(re.sub(r'[^0-9]', '', name))
    )
    return last


def specific_net_epoch(results_dir, epoch):
    return '%s/network_file_%d.pkl' % (results_dir, epoch)



def load_net(network_path):
    print 'Loading from', network_path
    with open(network_path) as f:
        return cPickle.load(f)


def visualize_control(mdp, controller, name='mdp_act'):
    """Visualize the actions of the controller as a heat map.

    mdp: some instance of LQRMDP
    controller: a function that returns the discrete control to take in that state.
    """
    min_state = mdp.min_state
    max_state = mdp.max_state
    x = np.linspace(min_state, max_state, num=200)
    y = np.linspace(min_state, max_state, num=200)
    xv, yv = np.meshgrid(x, y)
    control = np.zeros((200, 200))
    # When drawing, this starts at the top left corner
    # Thus, (0,0) needs to correspond to (minx, maxy), which means
    # the indexing needs to be flipped for the y-coordinate
    for i in xrange(200):
        for j in xrange(200):
            state = np.array([xv[i,j], yv[200-1-i,200-1-j]])
            control[i,j] = controller(state)
    plt.figure()
    plt.imshow(control)
    plt.clim(min(mdp._control), max(mdp._control))
    plt.colorbar()
    # plt.show()
    plt.savefig(name + '.png')
    """
    with open(name, 'w') as f:
        cPickle.dump(control, f)
    """


def optimal_discrete_controller(mdp):
    K = mdp.optimal_control()
    def controller(state):
        control = np.dot(K, state)
        return mdp._round_control(control)
    return controller


def optimal_greedy_controller(mdp):
    # The optimal controller uses a cost to go function
    # The method used here biases towards a greedy function
    # What if those two are not the same?
    def controller(state):
        costs = [mdp._cost(state, a) for a in mdp._control]
        return np.argmax(costs)
    return controller


def network_controller(mdp, network):
    def controller(state):
        state = state.astype(floatX)
        q_vals = network.q_vals(state)
        # Q vals is a D x G array, roughly
        action = tuple(
            np.argmax(q_v) for q_v in q_vals
        )
        # Turn into control
        return mdp._action_to_control(action)
    return controller

def quad_network_controller(mdp, network):
    def controller(state):
        state = state.astype(floatX)
        N = len(state)
        obs = np.zeros(2 * N + N * (N - 1) // 2)
        obs[:N] = state
        obs[N:2*N] = state ** 2
        ind = 2 * N
        for xi, xj in itertools.combinations(state, 2):
            obs[ind] = xi * xj
            ind += 1

        q_vals = network.q_vals(obs)
        # Q vals is a D x G array, roughly
        action = tuple(
            np.argmax(q_v) for q_v in q_vals
        )
        # Turn into control
        return mdp._action_to_control(action)
    return controller


def sanity_check(mdp, network):
    min_state = mdp.min_state
    max_state = mdp.max_state
    x = np.linspace(min_state, max_state, num=200)
    y = np.linspace(min_state, max_state, num=200)
    vals = set()
    for i in x:
        for j in y:
            vals.add(np.argmax(network.q_vals([i, j])[0]))
    return vals


def linear_controller(mdp, bias, weights):
    # Only works for 1 dimensional control
    def controller(state):
        state = state.astype(floatX)
        N = len(state)
        obs = np.zeros(2 * N + N * (N - 1) // 2)
        obs[:N] = state
        obs[N:2*N] = state ** 2
        ind = 2 * N
        for xi, xj in itertools.combinations(state, 2):
            obs[ind] = xi * xj
            ind += 1
        obs = obs.reshape(1, -1)
        qvals = bias + np.dot(obs, weights)[0]
        action = np.argmax(qvals)
        return mdp._action_to_control( (action,) )
    return controller



if __name__ == '__main__':
    results_dir = 'lqr_results'
    mdp_path = 'saved_%s/mdp' % results_dir
    mdp = load_mdp(mdp_path)
    opt = optimal_discrete_controller(mdp)

    with open('dumpingground') as f:
        params = cPickle.load(f)
    bias = params['bias'].get_value()
    weights = params['weights'].get_value()

    visualize_control(mdp, opt, 'mdp_act_opt')
    visualize_control(mdp, linear_controller(mdp, bias, weights), 'simple_linear_heatmap')
    print 1/0

    opt_g = optimal_greedy_controller(mdp)
    visualize_control(mdp, opt, 'mdp_act_opt')
    visualize_control(mdp, opt_g, 'mdp_act_opt_g')
    network = load_net(specific_net_epoch(results_dir, 0))
    visualize_control(mdp, network_controller(mdp, network))
    print 1/0

    for e in range(1,41):
        network = load_net(specific_net_epoch(results_dir, e))
        nn = quad_network_controller(mdp, network)
        visualize_control(mdp, nn, 'linear_still_bad_heatmaps/epoch_%d' % e)

    """
    while not os.path.isfile(specific_net_epoch(results_dir, 40)):
        print 'Waiting for experiment to finish...'
        time.sleep(30)

    for e in range(1, 21):
        network = load_net(specific_net_epoch(results_dir, e))
        nn = network_controller(mdp, network)
        visualize_control(mdp, nn, 'refined/mdp_act_relu_nn_%d' % e)
    """

