import numpy as np
import theano

import cPickle
import pdb
import random
import time

from mdp_utils import MDP
from lqr_deep_q import q_network
from lqr_deep_q import lqr_agent
from lqr_deep_q import lqr_experiment

floatX = theano.config.floatX


def LQR_problem(state_dim, control_dim):
    """ Generate a random LQR problem. """
    # entries range in +/- 1
    A = 2 * np.random.rand(state_dim, state_dim) - 1
    B = 2 * np.random.rand(state_dim, control_dim) - 1

    # Check system is controllable
    control_check = np.concatenate(
        [np.dot(np.linalg.matrix_power(A, i), B) for i in range(state_dim)],
        axis=1
    )
    if np.linalg.matrix_rank(control_check) != state_dim:
        # try again
        return LQR_problem(state_dim, control_dim)

    # Generate random costs
    # costs entries are in range +/- 1
    q_base = 2 * np.random.rand(state_dim, state_dim) - 1
    Q = np.dot(q_base.T, q_base)
    r_base = 2 * np.random.rand(control_dim, control_dim) - 1
    R = np.dot(r_base.T, r_base)

    return (A, B, Q, R)


class LQRMDP(MDP):
    def __init__(self, A, B, Q, R, granularity, rng=None):
        self.A = A
        self.B = B
        self.Q = Q
        self.R = R
        self.state_dim = Q.shape[0]
        self.control_dim = R.shape[0]
        self.granularity = granularity
        if rng is not None:
            self.rng = rng
        else:
            self.rng = np.random.RandomState()
        min_control = -2
        max_control = 2
        self._control = np.linspace(
            min_control, max_control, num=self.granularity
        )
        self.min_state = -10
        self.max_state = 10
        # Get some random costs for later normalization
        costs = []
        n_samples = 10000
        for _ in xrange(n_samples):
            state = self.random_start_state()
            control = self.rng.choice(self._control, self.control_dim)
            costs.append(self._cost(state, control))
        self.cost_range = max(costs) - min(costs)
        self.avg_random_cost = sum(costs) / float(n_samples)

    def _cost(self, state, control):
        return np.dot(state.T, np.dot(self.Q, state)) + \
                np.dot(control.T, np.dot(self.R, control))

    def _action_to_control(self, action):
        return self._control[action]

    def next_state(self, state, action):
        # Expects an action tuple (a0, a1, a2, ...), one for each control dimension
        action = list(action)
        vec = self._action_to_control(action)
        reward, next_state = self._next_state_raw_control(state, vec)
        return (state, action, reward, next_state)

    def _next_state_raw_control(self, state, control):
        next_state = np.dot(self.A, state) + np.dot(self.B, control)
        cost = self._cost(state, control)
        # max reward = min cost
        # Normalize cost to make it approximately (-1, 1)
        cost = (cost - self.avg_random_cost) / (self.cost_range * 0.5)
        return (-cost, next_state)

    def _round_control(self, control):
        # Rounds the given control to the closest one allowed for the
        # given granularity
        rounded = np.zeros(self.control_dim)
        for i in range(self.control_dim):
            v = control[i]
            rounded[i] = min(self._control, key=lambda x: abs(x - v))
        return rounded

    def should_end_episode(self, state):
        """ To prevent cost blowup, terminate if any entry
        goes past +/- 10.
        """
        return np.min(state) < self.min_state or np.max(state) > self.max_state

    def optimal_control(self):
        # Return steady state K
        # Note the action to take is Kx, not K!
        A,B,Q,R = self.A, self.B, self.Q, self.R
        P = np.zeros((self.state_dim, self.state_dim))
        K = None
        for _ in xrange(10000):
            inv = -np.linalg.inv(R + np.dot(B.T, np.dot(P, B)))
            K = np.dot(inv, np.dot(B.T, np.dot(P, A)))
            temp = A + np.dot(B, K)
            temp = np.dot(temp.T, np.dot(P, temp))
            P = Q + np.dot(K.T, np.dot(R, K)) + temp
        return K

    def random_start_state(self):
        # Return a reasonable start state to use for learning
        return 0.3 * self.rng.randn(self.state_dim)


if __name__  == '__main__':
    state_dim = 2
    control_dim = 1
    granularity = 51

    # Used fixed RNG seeds for reproduction
    rng = np.random.RandomState(12345)

    """
    A,B,Q,R = LQR_problem(state_dim, control_dim)
    mdp = LQRMDP(A,B,Q,R,granularity)
    """
    mdp_path = 'saved_lqr_results/mdp'
    with open(mdp_path) as f:
        mdp = cPickle.load(f)
    # Override the MDP's RNG with the one used for this test run
    # (This lets us load the MDP from the same file.)
    mdp.rng = rng

    # Store the MDP this was tested on
    with open('lqr_results/mdp', 'w') as f:
        cPickle.dump(mdp, f, -1)

    action_shape = (granularity,) * control_dim
    network_type = 'lqr'
    if 'quad' in network_type:
        # x_i, x_i^2, x_ix_j
        n_features = 2 * state_dim + state_dim * (state_dim - 1) // 2
    else:
        n_features = state_dim

    # TODO label these hyperparams
    q_net = q_network.DeepQLearner(
        n_features,
        action_shape,
        0.99,
        .1,
        .99,
        1e-6,
        0,
        1000,
        -1,
        64,
        network_type,
        'rmsprop',
        'mean',
        rng
    )
    n_epochs = 40
    steps_per_epoch = 50000
    steps_per_test = 10000
    replay_memory_size = 10 ** 6
    replay_start_size = 100

    """
    start_state = mdp.random_start_state()
    """
    with open('saved_lqr_results/start_state') as f:
        start_state = cPickle.load(f)

    with open('lqr_results/start_state', 'w') as f:
        cPickle.dump(start_state, f, -1)

    # Decay over the first 33% of actions
    agent = lqr_agent.LQRAgent(q_net, 1., .1, steps_per_epoch * n_epochs // 3,
                               replay_memory_size, replay_start_size,
                               rng)
    experiment = lqr_experiment.LQRExperiment(mdp, agent, n_epochs, steps_per_epoch,
                                              steps_per_test,
                                              rng,
                                              start_state=start_state,
                                              network_type=network_type)
    experiment.run()

