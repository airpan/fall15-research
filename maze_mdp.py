"""
Functions for generating a random 3d maze MDP.
"""
import random
import numpy as np
import mdp_utils


class DisjointSet(object):
    def __init__(self, n):
        self.parents = np.zeros(n)
        self.parents.fill(-1)

    def find(self, x):
        # Return root for set containing x
        if self.parents[x] < 0:
            return x
        self.parents[x] = self.find(self.parents[x])
        return self.parents[x]

    def union(self, x, y):
        root1 = self.find(x)
        root2 = self.find(y)
        if root1 == root2:
            return

        size1 = -self.parents[root1]
        size2 = -self.parents[root2]
        if size1 < size2:
            self.parents[root1] = root2
            self.parents[root2] = -(size1+size2)
        else:
            self.parents[root2] = root1
            self.parents[root1] = -(size1+size2)


def generate_course(L, W):
    """
    Generates a course of size L x W.

    In a course, we want a path going from (0,0) to (L-1, W-1).

    2 degrees of freedom, We can add -1,0,1 to each of X,Y (so diagonal moves
    are allowed.)

    The course has some number of obstacles. For horizontal + vertical movement,
    path blocking is straightforward. For diagonal movement, we treat diagonal
    moves as a combination of two cardinal moves. To go down-right, one of
    the paths down + right, right + down must be doable.

    Course generation happens by random wall smashing.

    Returns: The tuple (T_sa, R_sa).
        - T_sa is an array S*A -> S
        - R_sa is an array S*A -> R, with R(s, a) = r
            - Rewards are done as: +1 for hitting the end, -10
              for taking an illegal action. (We do this to make sure
              every action is possible at every timestep, and rely on the RL
              to avoid doing these actions.)

    """
    cells = DisjointSet(L * W)
    walls = set()
    for x in xrange(L):
        for y in xrange(W):
            # numbering system is 0 -> (0,0), 1 -> (0,1), W-1 -> (0,W-1), W -> (1, 0)
            # make sure pairs are sorted!!!

            # Only need to check walls going left or up
            base = x * W + y
            if y - 1 >= 0:
                walls.add( (base-1, base) )
            if x - 1 >= 0:
                walls.add( (base-W, base) )
    walls = list(walls)
    random.shuffle(walls)

    # valid_actions[s,a] = state when taking action a from s, with L*W as failure
    # 0 1 2
    # 3 x 5
    # 6 7 8
    # (0,0) is top left corner)
    # right, down are increasing
    # action 4 always goes to sink state
    valid_actions = np.zeros((L*W + 1, 9), dtype='i')
    valid_actions.fill(L*W)

    while cells.find(0) != cells.find(L * W - 1):
        start, end = walls.pop()
        cells.union(start, end)
        # do cardinal directions here. Handle diagonals at the end
        if end - start == 1:
            valid_actions[start, 5] = end
            valid_actions[end, 3] = start
        elif end - start == W:
            valid_actions[start, 7] = end
            valid_actions[end, 1] = start
        else:
            raise ValueError("Error in course generation. Probable bug")

    paths = [(0,-W-1,1,3), (2,-W+1,1,5), (6,W-1,3,7), (8,W+1,5,7)]
    for i in xrange(L*W):
        for diag, offset, a1, a2 in paths:
            if valid_actions[valid_actions[i, a1], a2] != L*W:
                valid_actions[i, diag] = i + offset
            if valid_actions[valid_actions[i, a2], a1] != L*W:
                valid_actions[i, diag] = i + offset

    R_sa = np.zeros((L*W+1, 9))
    for i in xrange(L*W):
        for a in range(9):
            if valid_actions[i, a] == L*W:
                R_sa[i, a] = -10
            elif valid_actions[i, a] == L*W - 1:
                R_sa[i, a] = 1

    return valid_actions, R_sa


def visualize(transitions, pi, L, W):
    """
    Visualizes how policy pi moves in the given course.

    -------------
    |11|13|15|..|
    -------------
    |31|33|..|..|
    -------------

    |, - are separators
    * are walls
    cells are a combination of <>^v
    """
    def row(a, b):
        cells = [b, a] * W
        cells.append(b)
        return cells

    grid = []
    # Init grid
    for i in xrange(L):
        grid.append(row('--', '-'))
        grid.append(row('..', '|'))
    grid.append(row('--', '-'))

    # Add actions
    for i in xrange(L*W):
        x, y = i // W, i % W
        ax = '< >'
        ay = '^ v'
        a = pi[i]
        a_str = ay[a // 3] + ax[a % 3]
        grid[1+2*x][1+2*y] = a_str

    # Modify walls
    for i in xrange(L*W):
        x, y = i // W, i % W
        if transitions[i][1] != L*W:
            grid[2*x][1+2*y] = '  '
        if transitions[i][3] != L*W:
            grid[1+2*x][2*y] = ' '
        if transitions[i][5] != L*W:
            grid[1+2*x][2+2*y] = ' '
        if transitions[i][7] != L*W:
            grid[2+2*x][1+2*y] = '  '

    print '\n'.join(''.join(row) for row in grid)



if __name__ == '__main__':
    le = 4
    wi = 6
    transitions, rewards = generate_course(le, wi)
    pi = mdp_utils.policy_iteration(transitions, rewards, 0.9, 100)
    visualize(transitions, pi, le, wi)
