import cPickle
import numpy as np
from lqr_deep_q import q_network


def load_net(filepath):
    with open(filepath) as f:
        return cPickle.load(f)
