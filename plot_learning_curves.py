import matplotlib.pyplot as plt
import numpy as np
import utils

def _read_results_data(results_csv):
    # (epoch, num_episodes, total_reward, reward_per_episode, mean_q)
    STEPS_PER_EPOCH = 50000.  # TODO output mean episode length in the CSV
    mean_episode_length = []
    reward_per_episode = []
    mean_q = []

    for row in utils.load_file(results_csv):
        epoch, ne, tr, rpe, mq = row
        epoch = int(epoch)
        ne = int(ne)
        tr = float(tr)
        rpe = float(rpe)
        mq = float(mq)
        mean_episode_length.append(STEPS_PER_EPOCH / ne)
        reward_per_episode.append(rpe)
        mean_q.append(mq)
    # Will get last epoch from for loop
    n_epochs = epoch

    return {
        'n_epochs': n_epochs,
        'mean_episode_length': mean_episode_length,
        'reward_per_episode': reward_per_episode,
        'mean_q': mean_q,
    }


def results_plot(results_csv):
    data = _read_results_data(results_csv)
    n_epochs = data['n_epochs']
    mean_episode_length = data['mean_episode_length']
    reward_per_episode = data['reward_per_episode']
    mean_q = data['mean_q']

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Mean Episode Length")
    plt.plot(range(n_epochs), mean_episode_length)
    plt.show()

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Average Reward")
    plt.plot(range(n_epochs), reward_per_episode)
    plt.show()

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Average Q value")
    plt.plot(range(n_epochs), mean_q)
    plt.show()


def overlayed_results_plot(csv1, csv2):
    data = _read_results_data(csv1)
    n_epochs1 = data['n_epochs']
    mean_episode_length1 = data['mean_episode_length']
    reward_per_episode1 = data['reward_per_episode']
    mean_q1 = data['mean_q']

    data = _read_results_data(csv2)
    n_epochs2 = data['n_epochs']
    mean_episode_length2 = data['mean_episode_length']
    reward_per_episode2 = data['reward_per_episode']
    mean_q2 = data['mean_q']

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Mean Episode Length")
    plt.plot(range(n_epochs1), mean_episode_length1)
    plt.plot(range(n_epochs2), mean_episode_length2)
    plt.legend(['Joint', 'Factored'])
    plt.show()

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Average Reward")
    plt.plot(range(n_epochs1), reward_per_episode1)
    plt.plot(range(n_epochs2), reward_per_episode2)
    plt.legend(['Joint', 'Factored'])
    plt.show()

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Average Q value")
    plt.plot(range(n_epochs1), mean_q1)
    plt.plot(range(n_epochs2), mean_q2)
    plt.legend(['Joint', 'Factored'])
    plt.show()


def _read_loss_data(learn_csv):
    loss = []
    all_eps = []
    for row in utils.load_file(learn_csv):
        mean_loss, eps = row
        mean_loss = float(mean_loss)
        eps = float(eps)
        loss.append(mean_loss)
        all_eps.append(eps)

    return {
        'loss': loss,
        'all_eps': all_eps,
    }


def loss_curve(learn_csv):
    data = _read_loss_data(learn_csv)
    loss = data['loss']
    all_eps = data['all_eps']

    fig, ax1 = plt.subplots(figsize=(6.667,5))
    x_list = range(len(loss))
    ax1.plot(x_list, loss, 'b-')
    ax1.set_xlabel("Training Episodes")
    ax1.set_ylabel("Mean Loss", color='b')

    ax2 = ax1.twinx()
    ax2.plot(x_list, all_eps, 'r-')
    ax2.set_ylabel('Epsilon for action', color='r')
    ax2.set_ylim([0,1.])

    plt.show()


def overlayed_loss_curve(csv1, csv2):
    data = _read_loss_data(csv1)
    loss1 = data['loss']
    all_eps1 = data['all_eps']

    data = _read_loss_data(csv2)
    loss2 = data['loss']
    all_eps2 = data['all_eps']

    fig, ax1 = plt.subplots(figsize=(6.667,5))
    x_list1 = range(len(loss1))
    x_list2 = range(len(loss2))
    ax1.plot(x_list1, loss1)
    ax1.plot(x_list2, loss2)
    ax1.set_xlabel("Training Episodes")
    ax1.set_ylabel("Mean Loss")

    plt.legend(['Joint', 'Factored'])
    plt.show()


if __name__ == '__main__':
    control_base = '/home/alex/fall15-research/experiment_results/control_asteroids/asteroids_10-16-09-19_0p0002_0p95'
    factored_base = '/home/alex/fall15-research/experiment_results/factored_asteroids/asteroids_10-16-09-16_0p0002_0p95'

    overlayed_results_plot(control_base + '/results.csv', factored_base + '/results.csv')
    overlayed_loss_curve(control_base + '/learning.csv', factored_base + '/learning.csv')

    """
    print 'Control'
    results_plot(control_base + '/results.csv')
    loss_curve(control_base + '/learning.csv')

    print 'Factored'
    results_plot(factored_base + '/results.csv')
    loss_curve(factored_base + '/learning.csv')
    """
