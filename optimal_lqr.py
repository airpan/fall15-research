from lqr import LQRMDP
import cPickle
import numpy as np

mdp_path = 'lqr_results/mdp'

with open(mdp_path) as f:
    mdp = cPickle.load(f)

state_path = 'lqr_results/start_state'
with open(state_path) as f:
    start_state = cPickle.load(f)

total_rew = 0
total_ep_length = 0
# For fixed start state this is all determinisitic
n_episodes = 1

for i in xrange(n_episodes):
    print i
    state = start_state
    K = mdp.optimal_control()
    reward = 0
    steps = 0
    while steps < 100:
        control = np.dot(K, state)
        # Round this to the closest discrete control allowed
        # This rounds based on distance to the discrete control,
        # which may not be the one that minimizess the loss in reward
        # from doing the rounding. It should be close enough to be okay.
        control = mdp._round_control(control)
        rew, ns = mdp._next_state_raw_control(state, control)
        reward += rew
        state = ns
        steps += 1
        if mdp.should_end_episode(state):
            break
        # At this point, on any loop exist, steps = num actions taken = num_rewards received
    total_rew += reward
    total_ep_length += steps

print total_rew / float(n_episodes), 'reward per episode'
print total_ep_length / float(n_episodes), 'average episode length'


