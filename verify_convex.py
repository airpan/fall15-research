""" Me verifying whether a function is convex or not by seeing if there
are local minima/maxima. Very unrigorous. """
import numpy as np
import scipy.stats
import matplotlib

import matplotlib.pyplot as plt

def weight(s, a, s2, a2, b):
    # Some small regularization to avoid numerical issues
    return max(10 ** -14, scipy.stats.norm.pdf( ((s - s2) ** 2 + (a - a2) ** 2) / b ))

def create_func(states, actions, values, b):
    """ Function is of form

    w_i * v_i / sum_i w_i
    """
    def func(s, a):
        weights = [weight(s, a, s2, a2, b) for s2, a2 in zip(states, actions)]
        weights = [w / sum(w for w in weights) for w in weights]
        return sum(w * v for w,v in zip(weights, values))
    return func


def inverse_weights(s, a, s2, a2, b):
    dist = (s - s2) ** 2 + (a - a2) ** 2
    if dist < 10 ** -14:
        dist = 10 ** -14
    return 1 / dist + b


def inverse_kernel(states, actions, values, b):
    def func(s, a):
        weights = [inverse_weights(s, a, s2, a2, b) for s2, a2 in zip(states, actions)]
        weights = [w / sum(w for w in weights) for w in weights]
        return sum(w * v for w,v in zip(weights, values))
    return func


def show_values(func, samp_s, samp_a, samp_v, figname=None):
    # Assume states and actions are all in (-2, +2)
    min_value = -2
    max_value = 2
    states = np.linspace(min_value, max_value, num=50)
    actions = np.linspace(min_value, max_value, num=50)
    xv, yv = np.meshgrid(states, actions)

    # When drawing, this starts at the top left corner
    # Thus, (0,0) needs to correspond to (minx, maxy), which means
    # the indexing needs to be flipped for the y-coordinate
    values = np.zeros((50, 50))
    for i in xrange(50):
        for j in xrange(50):
            values[i,j] = func(xv[i,j], yv[50-1-i,50-1-j])
    plt.figure()
    plt.imshow(values, extent=(min_value, max_value, min_value, max_value), cmap=plt.cm.cubehelix)
    plt.colorbar()
    plt.scatter(samp_s, samp_a, s=np.pi * 5 ** 2, c=samp_v, cmap=plt.cm.cubehelix)

    if figname is None:
        plt.show()
    else:
        plt.savefig(figname + '.png')



if __name__ == '__main__':
    states = [1.70571393,1.41355874,-0.66129309,1.5281825,-1.22809608,-1.23999698,0.40054302,0.65275586,-1.66428412,-1.95268841]
    actions = [-1.19421184,1.97068142,-0.20122753,-1.97089225,-0.01116175,-0.97823814,-0.12818329,0.55366677,-0.72759034,-1.03577431]
    values = [-0.38656692,0.15114003,0.36342047,-0.35118148,-0.26179931,0.48005728,0.15178463,0.27046044,-0.15857919,-0.19685288]

    b = 0
    func = inverse_kernel(
        states, actions, values, b
    )
    show_values(func, states, actions, values, figname=str(b).replace('.','_'))
