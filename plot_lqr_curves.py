import matplotlib.pyplot as plt
import numpy as np
import plot_learning_curves
import utils


def results_plot(results_csv):
    # (epoch, num_episodes, MSE, total_reward, reward_per_episode)
    mean_episode_length = []
    reward_per_episode = []

    for row in utils.load_file(results_csv):
        epoch, ne, mse, tr, rpe, mq = row
        epoch = int(epoch)
        ne = int(ne)
        mse = float(mse)
        tr = float(tr)
        rpe = float(rpe)
        mq = float(mq)
        mean_episode_length.append(mse)
        reward_per_episode.append(rpe)
    # Will get last epoch from for loop
    n_epochs = epoch

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Mean Episode Length")
    plt.plot(range(n_epochs), mean_episode_length)
    plt.show()

    plt.figure(figsize=(6.667,5))
    plt.xlabel("Training Epochs")
    plt.ylabel("Average Reward")
    plt.plot(range(n_epochs), reward_per_episode)
    plt.show()


if __name__ == '__main__':
    lqr_base = '/home/alex/fall15-research/discounted_tanh_lqr_results'
    results_plot(lqr_base + '/results.csv')
    plot_learning_curves.loss_curve(lqr_base + '/learning.csv')
