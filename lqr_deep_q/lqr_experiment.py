"""The ALEExperiment class handles the logic for training a deep
Q-learning agent in the Arcade Learning Environment.

Author: Nathan Sprague

"""
import itertools
import logging
import numpy as np
import sys
import theano

floatX = theano.config.floatX

# Set logger to print to stdout
logging.getLogger('').addHandler(logging.StreamHandler(sys.stdout))


class LQRExperiment(object):
    def __init__(self, mdp, agent, num_epochs, epoch_length, test_length,
                 rng, start_state=None, network_type=None):
        self.mdp = mdp
        self.agent = agent
        self.num_epochs = num_epochs
        self.epoch_length = epoch_length
        self.test_length = test_length
        self.rng = rng
        self.start_state = start_state
        self.network_type = network_type

        self.buffer_length = 2
        self.buffer_count = 0

    def run(self):
        """
        Run the desired number of training epochs, a testing epoch
        is conducted after each training epoch.
        """
        # Kludge - finishing epoch 0 tells the agent to save the
        # initial net
        self.agent.finish_epoch(0)
        for epoch in range(1, self.num_epochs + 1):
            self.run_epoch(epoch, self.epoch_length)
            self.agent.finish_epoch(epoch)

            if self.test_length > 0:
                self.agent.start_testing()
                self.run_epoch(epoch, self.test_length, True)
                self.agent.finish_testing(epoch)

    def run_epoch(self, epoch, num_steps, testing=False):
        """ Run one 'epoch' of training or testing, where an epoch is defined
        by the number of steps executed.  Prints a progress report after
        every trial

        Arguments:
        epoch - the current epoch number
        num_steps - steps per epoch
        testing - True if this Epoch is used for testing and not training

        """
        # Going to cap episode length to 100 at most
        max_episode_length = 100
        steps_left = num_steps
        while steps_left > 0:
            prefix = "testing" if testing else "training"
            print(prefix + " epoch: " + str(epoch) + " steps_left: " +
                         str(steps_left))
            _, num_steps = self.run_episode(steps_left, max_episode_length, testing)

            steps_left -= num_steps


    def _init_episode(self):
        """ This method resets the game if needed, performs enough null
        actions to ensure that the screen buffer is ready and optionally
        performs a randomly determined number of null action to randomize
        the initial game state."""
        # Trying a forced start state for every epoch
        if self.start_state is not None:
            self.cur_state = self.start_state.astype(floatX)
        else:
            self.cur_state = self.mdp.random_start_state()

    def _act(self, action):
        _, __, reward, next_state = self.mdp.next_state(self.cur_state, action)
        self.cur_state = next_state
        return reward

    def _obs(self):
        # Get the observations for the current state
        if 'quad' in self.network_type:
            # Quadratic setup
            N = self.cur_state.shape[0]
            obs = np.zeros(N + N + N * (N-1) / 2)
            obs[:N] = self.cur_state
            obs[N:2*N] = self.cur_state ** 2
            # and the pairwise choices
            ind = 2 * N
            for xi, xj in itertools.combinations(self.cur_state, 2):
                obs[ind] = xi * xj
                ind += 1
            return obs
        # Use default
        return self.cur_state

    def run_episode(self, max_steps, max_episode_length, testing):
        """Run a single training episode.

        The boolean terminal value returned indicates whether the
        episode ended because the game ended or the agent died (True)
        or because the maximum number of steps was reached (False).
        Currently this value will be ignored.

        Return: (terminal, num_steps)
        """
        self._init_episode()
        action = self.agent.start_episode(self._obs())
        num_steps = 0
        while True:
            reward = self._act(action)
            terminal = self.mdp.should_end_episode(self.cur_state)
            num_steps += 1

            if num_steps >= max_episode_length:
                terminal = True

            if terminal or num_steps >= max_steps:
                self.agent.end_episode(reward, terminal)
                break


            action = self.agent.step(reward, self._obs())
        return terminal, num_steps

