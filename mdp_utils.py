"""Utilities for MDPs with deterministic transitions."""

import numpy as np


class MDP(object):
    """ Abstraction to represent the dynamics of an MDP.

    Implementing transitions, rewards, discounting, etc.
    is the job of the subclass.
    """
    def __init__(self):
        pass

    def next_state(self, state, action):
        """ Should return the tuple (state, action, reward, next_state). """
        raise NotImplementedError


def compute_vpi(pi, T, R, gamma):
    """
    ASSUMES DISCRETE STATES AND ACTIONS

    pi: deterministic S -> A array
    T: S*A -> S array for transitions
    R: S*A -> R array for rewards
    gamma: discount factor
    """
    nS = pi.shape[0]
    # Build system of equations
    A = np.identity(nS)
    for i in xrange(nS):
        nextstate = T[i, pi[i]]
        A[i, nextstate] -= gamma
    b = np.zeros(nS)
    for i in xrange(nS):
        b[i] = R[i, pi[i]]
    vpi = np.linalg.solve(A, b)
    assert vpi.shape == (nS,), 'Got shape %s' % str(vpi.shape)
    return vpi


def compute_qpi(vpi, T, R, gamma):
    """
    vpi: value func for pi, S -> A
    T: transitions S*A -> S
    R: rewards S*A -> R
    gamma: discount factor
    """
    nS = T.shape[0]
    nA = T.shape[1]
    qpi = np.zeros((nS, nA))
    for s in xrange(nS):
        for a in xrange(nA):
            qpi[s, a] = R[s, a] + gamma * vpi[T[s,a]]
    return qpi


def tuple_to_num(a_tup, action_shape):
    factor = 1
    num = 0
    for a, N in reversed(zip(a_tup, action_shape)):
        num += a * factor
        factor *= N
    return num


def num_to_tuple(num, action_shape):
    tup = []
    for N in reversed(action_shape):
        tup.append(num % N)
        num //= N
    return tuple(reversed(tup))


def compute_factored_qpi(vpi, T, R, gamma, action_shape):
    """
    Do Q-value iteration with a factored action space.

    Arguments are same as compute_qpi, except for action_shape.

    action_shape: A D-length tuple, where D is the number
        of degrees of freedom and a_i is the number of buckets
        action a is discretized into.
    """
    raise NotImplementedError


def factored_policy_iteration(T, R, gamma, n_iter):
    raise NotImplementedError


def policy_iteration(T, R, gamma, n_iter):
    pi_prev = np.zeros(T.shape[0], dtype='i')

    for i in xrange(n_iter):
        vpi = compute_vpi(pi_prev, T, R, gamma)
        qpi = compute_qpi(vpi, T, R, gamma)
        pi = qpi.argmax(axis=1)
        pi_prev = pi

    return pi_prev
