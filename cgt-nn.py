# Playing around with cgt's nn module
import cgt
from cgt import nn
import numpy as np
import time

import utils


def simple_cnn(X, y, stepsize=10**-3):
    # X = incoming batches of images
    # y = labels

    # rectify(x) => maps x to x * (x >= 0)
    # SpatialConvolution: constructor call that builds weights and biases
    #   for the convolution (for the filters)
    conv1 = nn.SpatialConvolution(
        1, 32, kernelshape=(3,3), pad=(1,1),
        weight_init=nn.IIDGaussian(std=0.1)
    )(X)
    relu1 = nn.rectify(conv1)
    # max pooling
    pool1 = nn.max_pool_2d(relu1, kernelshape=(2,2), stride=(2,2))
    # 2nd conv layer
    conv2 = nn.SpatialConvolution(
        32, 32, kernelshape=(3,3), pad=(0,0),
        weight_init=nn.IIDGaussian(std=0.1)
    )(pool1)
    relu2 = nn.rectify(conv2)
    pool2 = nn.max_pool_2d(relu2, kernelshape=(2,2), stride=(2,2))
    # Flatten for the final layer
    batchsize, channels, rows, cols = pool2.shape
    pool2_flat = cgt.reshape(pool2, [batchsize, channels*rows*cols])
    nfeats = cgt.infer_shape(pool2_flat)[1]

    # Get final label predictions
    # nn.Affine = linear function
    # nn.logsoftmax = take softmax then log of linear output
    lin_output = nn.Affine(nfeats, 10)(pool2_flat)
    logprobs = nn.logsoftmax(lin_output)
    # This indexing does it over each image in the batch
    # (I think, not entirely sure)
    # Mean vs sum is equivalent for loss minimization, but mean
    # gives more comparability between batch sizes
    loss = -logprobs[cgt.arange(batchsize), y].mean()

    # Now, actually train this beast
    # nn.get_paramaters: walks through computation graph to find all
    # parameters
    params = nn.get_parameters(loss)
    print params
    gparams = cgt.grad(loss, params)
    updates = [(p, p-stepsize*gp) for p,gp in zip(params, gparams)]
    # Recall the function semantics
    # Takes [inputs], reports output, then for each (param, new val)
    # in updates, sets param to the new value
    updater = cgt.function([X, y], loss, updates=updates)
    evaluator = cgt.function([X], logprobs)

    return {'updater': updater,
            'evaluator': evaluator}


def init_weights(*shape):
    return cgt.shared(np.random.randn(*shape) * 0.01, fixed_shape_mask='all')


def train():
    mnist = utils.fetch_dataset('http://rll.berkeley.edu/cgt-data/mnist.npz')
    cgt.update_config(default_device=cgt.core.Device(devtype="cpu"), backend="native")

    # Normalize pixel intensities
    Xdata = (mnist['X']/255.).astype(cgt.floatX)
    ydata = mnist['y']

    np.random.seed(0)

    # Reshape into convolutional
    Xdata = Xdata.reshape(-1, 1, 28, 28)

    Xtrain = Xdata[0:60000]
    ytrain = ydata[0:60000]

    Xtest = Xdata[60000:70000]
    ytest = ydata[60000:70000]

    inds = np.random.permutation(60000)
    Xtrain = Xtrain[inds]
    ytrain = ytrain[inds]

    n_epochs = 10
    batch_size = 128

    # (batch size, n_channels, n_rows, n_cols)
    Xshape = (batch_size, 1, 28, 28)

    X = cgt.tensor4("X", fixed_shape=Xshape)
    y = cgt.vector("y", dtype='i8')

    model = simple_cnn(X, y)
    train = model['updater']
    peeker = model['evaluator']

    def get_loss(Xvalid, yvalid):
        # Abuse of names, these aren't really validation sets
        # TODO actually implement this in CGT instead of on the
        # compiled function
        probs = peeker(Xvalid)
        labels = np.argmax(probs, axis=1)
        diff = (probs != yvalid)
        return np.mean(diff)



    for i in xrange(n_epochs):
        print 'Epoch %d' % i
        tstart = time.time()
        for start in xrange(0, Xtrain.shape[0], batch_size):
            end = start + batch_size
            train(Xtrain[start:end], ytrain[start:end])
        print '%f seconds for epoch' % (time.time() - tstart)
        print 'Training err %f' % get_loss(
            Xtrain[:10000], ytrain[:10000]
        )
        print 'Test err %f' % get_loss(
            Xtest, ytest
        )

if __name__ == '__main__':
    train()
